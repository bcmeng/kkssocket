#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>

#define MAXLINE 4096

int main(int argc,char ** argv)
{
    int listenfd,connfd;
    struct sockaddr_in serveraddr;
    char buff[4096],sendline[4096];
    int n;
    if((listenfd=socket(AF_INET,SOCK_STREAM,0))==-1)
    {
        printf("socket creat wrong!");
        exit(0);
    }
    memset(&serveraddr,0,sizeof(serveraddr));
    serveraddr.sin_family=AF_INET;
    serveraddr.sin_addr.s_addr=htonl(INADDR_ANY);
    serveraddr.sin_port=htons(6666);
    
    printf("run !");
    if(bind(listenfd,(struct sockaddr*)&serveraddr,sizeof(serveraddr))
    ==-1)
    {
        printf("socket bind wrong!");
        exit(0);
    }

    if(listen(listenfd,10)==-1)
    {
        printf("socket listen wrong!");
        exit(0);
    }
    
    printf("wait client .....");
    
    while(1)
    {
        if((connfd==accept(listenfd,(struct sockaddr*)NULL,NULL))==-1)
        {
            printf("socket accept wrong!");
            continue;
        }
        n=recv(connfd,buff,MAXLINE,0);
        buff[n]='\0';
        printf("recvice from client %s\n",buff);
        fgets(sendline,4096,stdin);
        if(send(connfd,sendline,strlen(sendline),0)<0)
        {
          printf("wrong!");
        }
        close(connfd);
    }

    close(listenfd);

}  