#include<stdio.h>
#include<stdlib.h>
#include<string.h>
#include<errno.h>
#include<sys/types.h>
#include<sys/socket.h>
#include<netinet/in.h>

#define MAXLINE 4096

int main(int argc,char ** argv)
{
    int sockfd,n;
    struct sockaddr_in serveraddr;
    char recvline[4096],sendline[4096];
    
    if((sockfd=socket(AF_INET,SOCK_STREAM,0))<0)
    {
        printf("socket creat wrong!");
        exit(0);
    }
    memset(&serveraddr,0,sizeof(serveraddr));
    serveraddr.sin_family=AF_INET;
    serveraddr.sin_port=htons(6666);
    
    if(inet_pton(AF_INET,argv[1],&serveraddr.sin_addr)<=0)
    {
        printf("wrong!");
        exit(0);
    }

    if(connect(sockfd,(struct sockaddr*)&serveraddr,sizeof(serveraddr))
    ==-1)
    {
        printf("socket bind wrong!");
        exit(0);
    }

    
    
    printf("send to server..\n");
    fgets(sendline,4096,stdin);
    
    if(send(sockfd,sendline,strlen(sendline),0)<0)
    {
        printf("send wrong!");
    }
    recv(sockfd,recvline,4096,0);
    printf("recv from server %s",recvline);
    
    close(sockfd);
}

